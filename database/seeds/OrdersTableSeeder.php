<?php

use Illuminate\Database\Seeder;
use App\Coordenada;
use App\User;
use App\Direccion;
use App\Orden;
use App\Destinatario;
use Faker\Factory as Faker;
class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
        'peso',
      'descripcion', 
      'coordenadas_id', 
      'direccion_id', 
      'destinatario_id',
      'usuario_id',*/
        $faker = Faker::create();
        for ($i = 0; $i <= 40; $i++)
        {
            $direccion = Direccion::create([
                'descripcion' => $faker->text(150),
                'linea_1' => $faker->text(50),
                'linea_2' => $faker->text(50),
                'ciudad' => $faker->text(31),
                'pais' => $faker->text(31),
                'codigo_postal' => $faker->text(5),
            ]);
            $coordenadas = Coordenada::create([
                'latitud' => $faker->latitude($min = -90, $max = 90),
                'longitud' => $faker->longitude($min = -90, $max = 90),
            ])
            
            ;
            $destinatario = Destinatario::create([
                'nombre' => $faker->firstName(),
                'apellido' => $faker->lastName(),
            ]); 
            $orden = Orden::create([
                'peso' => $faker->randomFloat(2, 0, 1000),
                'descripcion' => $faker->text(150),
                'coordenadas_id' => $coordenadas->id,
                'direccion_id' => $direccion->id,
                'destinatario_id' => $destinatario->id,
                'usuario_id' => $faker->numberBetween($min = 1, $max = 5),
            ]);
            
        }
    }
}
