<?php

use Illuminate\Database\Seeder;
use App\User;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i=0; $i <= 20; $i++)
        {
            User::create([
                'nombre' => $faker->firstName(),
                'apellido' => $faker->lastName(),
                'fecha_nacimiento' => $faker->date('Y-m-d','now'),
                'email' => $faker->email(),
                'api_token' => str_random(60),
                'password' => str_random(20),
            ]);
        }
       
    }
}
