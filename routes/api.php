<?php

use Illuminate\Http\Request;
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['prefix' => 'v1', 'middleware' => 'cors'], function () 
{
    Route::get('users', ['uses' => 'UsersController@index']);
    Route::post('users/create', ['uses' => 'UsersController@create']);
    Route::post('login', ['uses' => 'LoginController@index']);
    Route::get('users/{id}/orders', ['uses' => 'OrdersController@index']);
    Route::post('users/{id}/orders/create', ['uses' => 'OrdersController@create']);
});
