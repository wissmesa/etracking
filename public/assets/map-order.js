$(document).ready(function ()
{
    L.mapquest.key = 'TtwwGj4iZfEfulRWXXJ8WIJFT3kkIy4Y';
    latitud = 6.4237499;
    longitud = -66.5897293;
    // SE CREA EL MAPA CON CENTRO EN VENEZUELA
    var map = L.mapquest.map('map', {
    center: [latitud, longitud],
    layers: L.mapquest.tileLayer('map'),
    zoom: 5
    });
    // CADA VEZ QUE EL USUARIO LE DA CLICK AL MAPA INVOCA UNA FUNCIÓN EL CUAL ESTA COMO
    // PARAMETROS LA LATITUD Y LONGITUD EXACTAS
    map.on('click', function(e) {
        loadMap(e.latlng.lat, e.latlng.lng);
    });
    // SE HACE UN MARCADOR Y SE AGREGA AL MAPA
    var marker = L.marker([latitud, longitud]).addTo(map);
    // SE MODIFICA LOS INPUTS
    document.getElementById('latitudFormOrder').value = latitud;
    document.getElementById('longitudFormOrder').value = longitud;
    function loadMap (lat, lng)
    {
        // BORRAR EL MARKER ANTERIOR
        marker.remove();
        // MODIFICA LOS VALORES DEL INPUT CON LA NUEVA COORDENADAS
        document.getElementById('latitudFormOrder').value = lat;
        document.getElementById('longitudFormOrder').value = lng;
        // SE AGREGA EL MARKER CON LAS NUEVAS COORDENADAS.
        marker = L.marker([lat, lng]).addTo(map); 
    }
});