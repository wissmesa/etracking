<?php

namespace App\Http\Controllers;
use App\User;
use App\Orden;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Hash;

//Controlador del Login
class LoginController extends Controller
{
    public function index (Request $request)
    {
    //Recibe el request
        if ($request->isJson())
        {
            //Verifica si el request es un json
            $data = $request->json()->all();
            //Guarda los datos del respectivo json
            if ($data)
            {
                //Se verifica si el json tiene datos.
                $user = User::where('email', $data['email'])->first();
                //Busca un usuario de acuerdo el email
                $ordenes = Orden::where('usuario_id', $user->id)->get();
                //Obtiene todas las ordenes por el id del usuario
                $user->ordenes = $ordenes;
                //Se crea un nuevo atributo json para las ordenes
                if($user && Hash::check($data['password'],$user->password))
                {
                    //Si el usuario existe verifica la contraseña del usuario ingresado
                    $response = Response::json($user, 201);
                    //Retorna toda la información a través de un json.
                    return $response;
                }
                else
                {
                    //Si el usuario no existe retorna un error
                    $noUser = Response::json(['Error' => 'No existe usuario con ese nombre'], 406);
                    return $noUser;
                }
            }
            else
            {
                //SI el json esta vacio retorna un error.
                $vacio = Response::json(['Error' => 'El JSON NO CONTIENE INFORMACIÓN'],401);
                return $vacio;
            }
            
        }
        //Si no es un json retorna un error.
        $error = Response::json(['Error' => 'Unauthorized'], 401);
        return $error;
    }
}
