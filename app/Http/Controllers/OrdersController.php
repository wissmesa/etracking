<?php

namespace App\Http\Controllers;
use App\User;
use App\Orden;
use App\Direccion;
use App\Coordenada;
use App\Destinatario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class OrdersController extends Controller
{
    function index(Request $request, $id) 
    {
        //Verifica si el request es un json
        if ($request->isJson())
        {
            //Busca un usuario a través el id de la ruta
            $user = User::where('id', $id)->first();
            if ($user){
                //Busca todas las ordenes a través del usuario_id
                $orders = Orden::where('usuario_id', $id)->get();
                //Se realiza un foreach para anidar la información
                foreach ($orders as $order)
                {
                    $direccion = Direccion::where('id', $order->id)->first();
                    $order->direccion = $direccion;
                    $coordenada = Coordenada::where('id', $order->id)->first();
                    $order->coordenada = $coordenada;
                    $destinatario = Destinatario::where('id', $order->id)->first();
                    $order->destinatario = $destinatario;
                }
                //Se realiza un json con la información solicitada
                $response = Response::json($orders,200);
                //Retorna el json
                return $response;
            }
            //Retorna un error si no encuentra el usuario
            $errorUser = Response::json(['Error' => 'No existe usuario'], 401);
            return $errorUser;
        }
        //REtorna un error si el request no es un json
        $error = Response::json(['Error' => 'Unauthorized'], 401);
        return $error;
    }

    function create (Request $request, $id)
    {
        // Verifica si el request es un Json
        if ($request->isJson())
        {
            //Extrae los datos del json
            $data = $request->json()->all();
            //Si el json no esta vacío.
            if($data)
            {
                //Crea los respectivos objetos de la información del json
                $direccion = Direccion::create([
                    'descripcion' => $data['direccion']['descripcion'],
                    'linea_1' => $data['direccion']['linea_1'],
                    'linea_2' => $data['direccion']['linea_2'],
                    'ciudad' => $data['direccion']['ciudad'],
                    'pais' => $data['direccion']['pais'],
                    'codigo_postal' => $data['direccion']['codigo_postal']
                ]);
                $coordenada = Coordenada::create([
                    'latitud' => $data['coordenada']['latitud'],
                    'longitud' => $data['coordenada']['longitud']
                ]);
                $destinatario = Destinatario::create([
                    'nombre' => $data['destinatario']['nombre'],
                    'apellido' => $data['destinatario']['apellido']
                ]);
                $orden = Orden::create([
                    'peso' => $data['peso'],
                    'descripcion' => $data['descripcion'],
                    'usuario_id' => $id,
                    'coordenadas_id' => $coordenada->id,
                    'destinatario_id' => $destinatario->id,
                    'direccion_id' => $direccion->id
                ]);
                $orden->direccion = $direccion;
                $orden->coordenadas = $coordenada;
                $orden->destinatario = $destinatario;
                $response = Response::json($orden, 201);
                //Retorna el json ya creado.
                return $response;    
            }
            else
            {
                // Si el json esta vacío ocurre...
                $vacio = Response::json(['Error' => 'El JSON no posee contenido']);
                return $vacio;
            }
        }
        //Si el request no es un json ocurre...
        $error = Response::json(['Error' => 'Unauthorized'], 401);
        return $error;
    }
}
