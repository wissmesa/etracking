<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;

class UsersController extends Controller
{
    public function index (Request $request)
    {
        //Pregunta si el request es un json
        if ($request->isJson())
        {
            //Extrae todos los usuarios
            $users = User::all();
            // Retorna el json con todos los usuarios
            $response = Response::json($users, 201);
            return $response;
        }
        $error = Response::json(['Error' => 'Unauthorized'], 401);
        return $error;
    }
    public function create (Request $request)
    {
        //Pregunta si el request es un json
        if ($request->isJson())
        {
            //Extrae los datos del json.
            $data = $request->json()->all();
            if ($data)
            {
                // Crea el usuario con la información del json
                $user = User::create([
                    'api_token' => str_random(60),
                    'nombre' => $data['nombre'],
                    'apellido' => $data['apellido'],
                    'fecha_nacimiento' => $data['fecha_nacimiento'],
                    'email' => $data['email'],
                    'password' => Hash::make($data['password'])          
                ]);
                //Retorna el usuario si fue creado con éxito.
                $response = Response::json($user,201);
                return $response;
            }
            else
            {
                //Si el json esta vacio retorna un error
                $vacio = Response::json(['Error' => 'El JSON no contiene información'],401);
                return $vacio;
            }
        }
        //Si el request no es un json retorna un error
        $error = Response::json(['Error' => 'Unauthorized'],401);
        return $error;
    }
}
