<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Destinatario extends Model
{
    //Tabla del modelo destinatario
    protected $table = 'destinatarios';
    //Campos rellenables en el modelo
    protected $fillable = [
        'nombre',
        'apellido',
    ];
}
