<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    //Tabla del modelo User
    protected $table = 'users';
    //Campos rellenables del modelo.
    protected $fillable = [
        'nombre', 
        'apellido',
        'email', 
        'password',
        'fecha_nacimiento',
        'api_token'
    ];
    protected $hidden = [
        'password', 
        'remember_token',
    ];
}
