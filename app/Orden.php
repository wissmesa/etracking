<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Orden extends Model
{
   //Tabla del modelo orden
   protected $table = 'ordenes';
   //Campos rellenables del modelo
   protected $fillable = [
      'peso',
      'descripcion', 
      'coordenadas_id', 
      'direccion_id', 
      'destinatario_id',
      'usuario_id',
   ];

}
