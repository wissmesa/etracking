<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coordenada extends Model
{
    //Tabla del modelo coordenada
    protected $table = 'coordenadas';
    //Campos rellenables en el modelo.
    protected $fillable = [
        'latitud',
        'longitud',
    ];
}
