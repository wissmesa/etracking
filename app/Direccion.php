<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Direccion extends Model
{
    //Tabla del modelo Direccion
    protected $table = 'direcciones';
    //Campos rellenables del modelo.
    protected $fillable = [
        'descripcion',
        'linea_1',
        'linea_2',
        'ciudad',
        'pais',
        'codigo_postal',
    ];

   /*public function orden ()
    {
        return $this->belongsTo('App\Orden','id');
    }*/
}
